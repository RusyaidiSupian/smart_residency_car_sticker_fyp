import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet
} from 'react-native';

import Camera from 'react-native-camera';

export default class CameraHome extends Component {

    constructor(props) {
        super(props);
        this.state = {
            qrcode: ''
        }
    }

    onBarCodeRead = (e) => this.setState({ qrcode: e.data });

    render() {
        return (
            <View style={styles.container}>
                <Camera
                    style={styles.preview}
                    onBarCodeRead={this.onBarCodeRead}
                    ref={cam => this.camera = cam}
                    aspect={Camera.constants.Aspect.fill}
                >
                    <Text style={{
                        backgroundColor: 'white',fontSize:32
                    }}>{this.state.qrcode}</Text>
                </Camera>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        width: 300,
    }
});