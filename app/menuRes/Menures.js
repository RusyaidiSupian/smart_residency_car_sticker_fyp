import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
//import Camera from 'react-native-camera';

import CameraHome from './CameraHome';
// import Profiles from '../components/Profiles';
import Report from './Report';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <CameraHome />
      </View>
    );
  }
}

// class Profiles extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Profiles />
//       </View>
//     );
//   }
// }
class Reports extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Report />
      </View>
    );
  }
}

class Menures extends Component {
  static navigationOptions = { title: 'Menures', header: null };
  render() {

    return (
      <UsersManager1 />
    );
  }
}

const UsersManager1 = createBottomTabNavigator({
  Home: HomeScreen,
  // Profiles: Profiles,
  ReportForm : Reports
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container2: {
    backgroundColor: '#455a64',
    flex: 1,
    flexDirection: 'row',
  },
  container1: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 30,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center'
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: 300,
}
});


export default Menures;