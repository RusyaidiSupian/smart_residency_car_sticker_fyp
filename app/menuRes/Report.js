import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import DatePicker from 'react-native-datepicker';

export default class FormRegister extends Component {

    constructor(props) {
        super(props);
        this.state = {
            date: { date: "2018-12-03" },
            report: '',
        }
    }
    submit = () => {
        const { date, report } = this.state;
        if (report == "") {
            alert("insert some report");
        } else {
            fetch('http://1427987.iium.acme.my/api/product/createreport.php', {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    date: date,
                    report: report,
                })
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson === "Done sent report" || responseJson === "Make Sure all have been insert") {
                        alert(responseJson.message);
                    } else {
                        alert(responseJson.message);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }
    render() {
        console.disableYellowBox = true;//remove the warning
        return (
            <View style={styles.container}>

                <View style={styles.container1}>
                    <Text style={styles.title}>Submit Your Report</Text>
                    <DatePicker
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        onDateChange={(date) => { this.setState({ date }) }}
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                    />
                    <TextInput style={styles.inputBox}
                        multiline={true}
                        numberOfLines={10}
                        maxLength={200}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Report here!!"
                        placeholderTextColor="#ffffff"
                        selectionColor="#fff"
                        //check number char
                        onChangeText={report => this.setState({ report })}
                    />
                    <Text>Character left: {this.state.report.length}/200</Text>
                    <TouchableOpacity style={styles.button} onPress={this.submit} >
                        <Text style={styles.buttonText}>Submit the Report</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        // height: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    title: {
        fontSize: 32,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    }
});