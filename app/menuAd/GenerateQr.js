import React, { Component } from 'react';
import QRCode from 'react-native-qrcode';
import { Text, View, StyleSheet, FlatList, TextInput, TouchableOpacity, Keyboard } from 'react-native';

export default class ViewData extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            dataSource: [],
            //refreshing: false,
        }
    }
    generate = () => {
        const { username } = this.state;
        fetch('http://1427987.iium.acme.my/api/product/readone.php', {
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: responseJson.records
                });
            })
            .catch((error) => {
                console.log(error)
            })
        Keyboard.dismiss();
    }
    //custom the layout of flatlist here
    renderItem = ({ item }) => {
        return (
            <View /**style={styles.container} warap with qrcode nnti*/>
                <View style={styles.container1} >
                    <Text style={styles.buttonText}> Data {item.id} </Text>
                    <Text style={styles.text}>Username: {item.username}</Text>
                    <Text style={styles.text}>Name: {item.name}</Text>
                    <Text style={styles.text}>Contact: {item.contact}</Text>
                    <Text style={styles.text}>Car Number: {item.no_plat}</Text>
                    <Text style={styles.text}>Status: {item.resident_status}</Text>
                    <Text style={styles.text}>Date Enrollment: {item.date_enrol}</Text>

                    <QRCode
                        value={item.username}
                        size={200}
                        bgColor='black'
                        fgColor='white'
                    >
                        <Text>{item.username}</Text>
                    </QRCode>
                </View>
            </View>
        )
    }
    render() {
        console.disableYellowBox = true;//remove the warning
        return (
            <View style={styles.container}>
                <View style={styles.container1}>
                    <Text style={styles.buttonText}>Generate QRCode</Text>
                    <TextInput style={styles.inputBox}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Username"
                        placeholderTextColor="#ffffff"
                        selectionColor="#fff"
                        onChangeText={(username) => this.setState({ username })}
                    />
                    <TouchableOpacity style={styles.button} onPress={this.generate.bind(this)} >
                        <Text style={styles.buttonText}>Generate</Text>
                    </TouchableOpacity>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={this.renderItem}
                    //keyExtractor={(item, index) => index}     
                    >
                    </FlatList>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        flexDirection: 'row',
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 30,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    },
    text:{
        fontSize:16,
        color:'#ffffff',
        //textAlign: 'center'
    },
    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    }
});