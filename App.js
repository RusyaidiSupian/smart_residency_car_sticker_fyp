
import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Login from './app/Login';
import Menuad from './app/menuAd/Menuad';
import Menures from './app/menuRes/Menures';


export default class App extends Component {
	render() {
		return (
			<UsersManager />
		);
	}
}

const UsersManager = StackNavigator({
	Home: { screen: Login },
	MenuAdmin: { screen: Menuad },
	MenuResident: { screen: Menures },
}, {
		navigationOptions: {
			header: false,
		}
	});
